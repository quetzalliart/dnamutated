// Diccionario de datos, palabras que no queremos que se repitan
const aaaa = 'AAAA'
const cccc = 'CCCC'
const gggg = 'GGGG'
const tttt = 'TTTT'

let mutated = false //Estado inicial
let arrayVertical = []

// Revisa que ninguno de los elementos del arreglo contenga alguno de los elementos del diccionario de datos
let verifyElements = (array) =>{
    try {
        mutated = false
        array.forEach((element) => {
            if(element.includes(aaaa)){
                mutated = true
                console.log(`El array contiene AAAA`);                
            }
            
            if(element.includes(cccc)){
                mutated = true
                console.log(`El array contiene CCCC`);                
            }
                
            if(element.includes(gggg)){
                mutated = true
                console.log(`El array contiene GGGG`);                
            }
                
            if(element.includes(tttt)){
                mutated = true
                console.log(`El array contiene TTTT`);                
            }            
        })
        if(mutated)
            return true
        return false
    }
    catch(e){
        throw e
    }              
}

let verticales = (dnaHorizontal) => {
    try{
        for(i=0; i < dnaHorizontal.length ; i++){
                arrayVertical[i] = ""
                // Recorre todo el arreglo variando solo en la posicion de la letra que toma para almacenar en el arreglo vertical
                // controlado por la variable i del ciclo externo
                dnaHorizontal.forEach((element)=> {
                    //console.log(element.charAt(i));        
                    arrayVertical[i] += element.charAt(i)
                })
            }

        return arrayVertical
    } catch(e) {
        throw e
    }    
}

module.exports.verifyElements = verifyElements;
module.exports.verticales = verticales;