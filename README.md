1. Clonar el repositorio
2. En una terminal descargar los paquetes:
    $ npm install express --save
    $ npm install body-parser
3. Correr la aplicacion
    node router.js
4. Mediante postman o similar llamar al endpoint:
    http://localhost:3000/mutation
    En el body se pueden probar estos dos casos con y sin mutación:
    POST /mutation
    {
    "dna":["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"]
    }
    {
    "dna":[ "ATGCGA", "CAGTGC", "TTATTT", "AGACGG", "GCGTCA","TCACTG" ]
    }