const utils = require('./utilidades.js')

let algoritmoCompleto = (dnaHorizontal) => {
    //Datos de prueba local
    //const dnaHorizontal = [ 'ATGCGA', 'CAGTGC', 'TTATGT', 'AGAAGG', 'CCCCTA','TCACTG' ] // Arreglo mutado
    //const dnaHorizontal = [ 'ATGCGA', 'CAGTGC', 'TTATTT', 'AGACGG', 'GCGTCA','TCACTG' ] // Arreglo no mutado 
    let mutacionHorizontal = false
    let mutacionVertical = false
 
    //Genera un arreglo con las columnas verticales del arreglo original
    let dnaVertical = utils.verticales(dnaHorizontal)
    //Imprime todas las palabras del arreglo vertical
    //dnaVertical.forEach((element)=> {console.log(element)}) 

    mutacionHorizontal = utils.verifyElements(dnaHorizontal);
    console.log(`La revision horizontal arroja: ${mutacionHorizontal}`);

    mutacionVertical = utils.verifyElements(dnaVertical)
    console.log(`La revision vertical arroja: ${mutacionVertical}`);

    if(mutacionHorizontal || mutacionVertical)
        return true
    return false
}

module.exports.algoritmoCompleto = algoritmoCompleto;