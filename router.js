const alg = require('./app')

const bodyParser = require('body-parser');
var express = require('express')
var app = express()
var router = express.Router()
var arregloPost

app.use(router)
app.use(bodyParser.json())

app.post('/mutation', (req, res) => {
    console.log(req.body.dna);
    var arregloPost = req.body.dna
    var resultado = alg.algoritmoCompleto(arregloPost)
    if(resultado)
        res.status(200).send('El adn si se encuentra mutado');
    res.status(403).send('El adn no se encuentra mutado');
})

app.listen(3000, function () {
  console.log('Ready')
})

module.exports = router;